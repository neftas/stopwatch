$(document).ready(function() {

    var timer = $("#timer"),
        startButton = $("#startButton"),
        stopButton = $("#stopButton"),
        resetButton = $("#resetButton"),
        offset,
        clock,
        interval,
        audioFile = new Audio("ding-sound.webm");

    startButton.on("click", start);
    stopButton.on("click", stop);
    resetButton.on("click", reset);

    reset();

    function enable(elem) {
        elem.prop("disabled", false);
        elem.removeClass("disabled");
    }

    function disable(elem) {
        elem.prop("disabled", true);
        elem.addClass("disabled");
    }

    function start() {
        if (!interval) {
            // needed to satisfy "only play audio on user interaction"
            audioFile.play();
            audioFile.pause();

            offset = Date.now();
            interval = setInterval(update, 1000);
            disable(startButton);
            enable(stopButton);
            disable(resetButton);
        }
    }

    function update() {
        clock += delta();
        render();
    }

    function render() {
        var date = new Date(clock);
        var hours = date.getUTCHours();
        var minutes = date.getUTCMinutes();
        var seconds = date.getUTCSeconds();
        timer.text(zeroPad(hours) + ":" + zeroPad(minutes) + ":" + zeroPad(seconds));

        var remainder = clock % 60000;
        var shouldPlaySound = clock > 0 && remainder >= 0 && remainder < 1000;
        if (shouldPlaySound) audioFile.play();
    }

    function stop() {
        if (interval) {
            clearInterval(interval);
            interval = null;
        }
        enable(startButton);
        disable(stopButton);
        enable(resetButton);
    }

    function reset() {
        clock = 0;
        enable(startButton);
        disable(stopButton);
        disable(resetButton);
        render();
    }

    function delta() {
        var now = Date.now(),
            d = now - offset;

        offset = now;
        return d;
    }

    function zeroPad(num) {
        if (num.toString().length > 2) throw "Number should have length between 1 and 2, but was '" + num + "'";
        return ("0" + num).length > 2 ? num : "0" + num;
    }
});

