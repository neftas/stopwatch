FROM busybox:1.30.1

COPY css/bootstrap.css css/stopwatch.css /stopwatch/css/
COPY js/bootstrap.js js/jquery-3.4.1.js js/popper.js js/stopwatch.js /stopwatch/js/
COPY ding-sound.webm index.html /stopwatch/

WORKDIR /stopwatch

CMD ["busybox", "httpd", "-v", "-f"]
